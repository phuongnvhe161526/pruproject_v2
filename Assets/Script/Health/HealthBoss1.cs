using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBoss1 : MonoBehaviour
{
    [Header("Health")]
    [SerializeField] private float start_health;
    public float current_health { get; private set; }
    private bool dead;
    private Animator animator;

    public bool drop;
    public GameObject theDrop;
    [SerializeField] GameObject win;

    private SpriteRenderer spriteRenderer;
    private bool invulnerable;

    [Header("Death Sound")]
    [SerializeField] private AudioClip deathSound;
    [SerializeField] private AudioClip hurtSound;

    public float GetStartHealth()
    {
        return start_health;
    }
    private void Awake()
    {
        current_health = start_health;
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void takeDame(float _dame)
    {
        current_health = Mathf.Clamp(current_health - _dame, 0, start_health);
        if (current_health > 0)
        {
            animator.SetTrigger("hurt");
            SoundManager.instance.PlaySound(hurtSound);
        }
        else
        {
            if (!dead)
            {
                animator.SetTrigger("die");

                win.SetActive(true);

                dead = true;
                if (drop)
                {
                    Instantiate(theDrop, transform.position, transform.rotation);
                }

                SoundManager.instance.PlaySound(deathSound);
            }


        }

    }
    public void AddHealth(float _value)
    {
        current_health = Mathf.Clamp(current_health + _value, 0, start_health);

    }

    private void Deactive()
    {
        gameObject.SetActive(false);
    }
}
