using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthColletion : MonoBehaviour
{
    [SerializeField] private float health_value;

    [SerializeField] private AudioClip pickupSound;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Health health = collision.GetComponent<Health>();
        if (collision.tag == "Player")
        {
        
            if(health.current_health < health.GetStartHealth())
            {
                SoundManager.instance.PlaySound(pickupSound);
                collision.GetComponent<Health>().AddHealth(health_value);
                gameObject.SetActive(false);
            }
           

        }
    }
}
