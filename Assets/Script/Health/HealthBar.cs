using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Health player_health;
    [SerializeField] private Image total_health_bar;
    [SerializeField] private Image current_health_bar;

    // Start is called before the first frame update
    void Start()
    {
        total_health_bar.fillAmount = player_health.current_health/3;
    }

    // Update is called once per frame
    void Update()
    {
        current_health_bar.fillAmount = player_health.current_health/3;
    }
}
