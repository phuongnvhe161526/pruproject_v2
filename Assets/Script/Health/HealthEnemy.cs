using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthEnemy : MonoBehaviour
{
    [Header("Health")]
    [SerializeField] private float start_health;
    public float current_health { get; private set; }
    private bool dead;
    private Animator animator;
    
    public bool drop;
    public GameObject theDrop;

    [Header("Iframes")]
    [SerializeField] private float iframes_duration;
    [SerializeField] private int number_of_flashes;
    private SpriteRenderer spriteRenderer;

    [Header("Components")]
    [SerializeField] private Behaviour[] components;
    private bool invulnerable;

    [Header("Death Sound")]
    [SerializeField] private AudioClip deathSound;
    [SerializeField] private AudioClip hurtSound;

    public float GetStartHealth()
    {
        return start_health;
    }
    private void Awake()
    {
        current_health = start_health;
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void takeDame(float _dame)
    {
        current_health = Mathf.Clamp(current_health - _dame, 0, start_health);
        if (current_health > 0)
        {
            animator.SetTrigger("hurt");
            StartCoroutine(Invunerability());
            SoundManager.instance.PlaySound(hurtSound);
        }
        else
        {
            if (!dead)
            {
                animator.SetTrigger("die");

                foreach (Behaviour component in components)
                {
                    component.enabled = false;
                }

                dead = true;
                if (drop)
                {
                    Instantiate(theDrop, transform.position, transform.rotation);
                }

                SoundManager.instance.PlaySound(deathSound);
            }


        }

    }
    public void AddHealth(float _value)
    {
        current_health = Mathf.Clamp(current_health + _value, 0, start_health);

    }
    public void RespawnHealth()
    {
        dead = false;
        AddHealth(start_health);
        animator.ResetTrigger("die");
        animator.Play("Idle");
        StartCoroutine(Invunerability());
    }
    private IEnumerator Invunerability()
    {
        Physics2D.IgnoreLayerCollision(8, 9, true);
        for (int i = 0; i < number_of_flashes; i++)
        {
            spriteRenderer.color = new Color(1, 0, 0, 0.5f);
            yield return new WaitForSeconds(iframes_duration / (number_of_flashes * 2));
            spriteRenderer.color = Color.white;
            yield return new WaitForSeconds(iframes_duration / (number_of_flashes * 2));
        }
        Physics2D.IgnoreLayerCollision(8, 9, false);
    }

    private void Deactive()
    {
        gameObject.SetActive(false);
    }
}
