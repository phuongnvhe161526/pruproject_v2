using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverGround : MonoBehaviour
{
    [SerializeField] private Health health;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            health.takeDame(3);
        }
    }
}
