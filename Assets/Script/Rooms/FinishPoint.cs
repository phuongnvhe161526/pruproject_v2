using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class FinishPoint : MonoBehaviour
{
    private UIManager uiManager;
    [SerializeField] private Health player_health;
    [SerializeField] private Image[] stars;
    [SerializeField] private Sprite goldenStarSprite;
    private int star;

    private void Awake()
    {
        uiManager = FindObjectOfType<UIManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player") {
            UnlockNewLevel();
            uiManager.Win();
            int score = Coins.Instance.score;
            PlayerPrefs.SetInt("Coins", score);
            PlayerPrefs.Save();
            star = Convert.ToInt32(player_health.current_health);
            if (star > PlayerPrefs.GetInt("stars" + (SceneManager.GetActiveScene().buildIndex-1), 0))
            {
                PlayerPrefs.SetInt("stars" + (SceneManager.GetActiveScene().buildIndex - 1), star);
                PlayerPrefs.Save();
            }
            for (int i = 0; i < star; i++)
            {
                stars[i].sprite = goldenStarSprite;
            }
        }

    }
    void UnlockNewLevel()
    {
        if(SceneManager.GetActiveScene().buildIndex >= PlayerPrefs.GetInt("ReachedIndex"))
        {
            
            PlayerPrefs.SetInt("ReachedIndex",SceneManager.GetActiveScene().buildIndex+1);
            PlayerPrefs.SetInt("UnlockedLevel",PlayerPrefs.GetInt("UnlockedLevel",1)+1);
            PlayerPrefs.Save();
        }
    }
}
