using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    [SerializeField] private Transform previousRoom;
    [SerializeField] private Transform nextRoom;
    [SerializeField] private Transform bossRoom;
    [SerializeField] private CameraController cam;
    [SerializeField] private Boss boss;
    [SerializeField] private GameObject wallDoor;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            if (collision.transform.position.x < transform.position.x)
            {
                if(bossRoom!=null)
                {
                    boss.isSameRoom = true;
                    boss.GetComponent<Animator>().SetTrigger("Boss_Idle");
                    wallDoor.SetActive(true);
                }
                cam.MoveToNewRoom(nextRoom);
            }else
            {
                cam.MoveToNewRoom(previousRoom);
            }
        }
    }
}
