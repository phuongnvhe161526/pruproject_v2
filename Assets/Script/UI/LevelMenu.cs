﻿using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelMenu : MonoBehaviour
{
    [SerializeField] private LevelObject[] levelObjects;
    [SerializeField] private GameObject[] locks;
    [SerializeField] private Sprite goldenStarSprite;
    private void Awake()
    {
        int unLockedLevel = PlayerPrefs.GetInt("UnlockedLevel", 1);
        for(int i = 0; i < levelObjects.Length; i++)
        {
            levelObjects[i].levelButton.interactable = false;
            locks[i].SetActive(true);
        }
        for (int i = 0; i <=unLockedLevel; i++)
        {
            levelObjects[i].levelButton.interactable = true;
            locks[i].SetActive(false);
           
            int stars = PlayerPrefs.GetInt("stars" + i.ToString(), 0);
            for (int j = 0; j < stars; j++)
            {
                levelObjects[i].starts[j].sprite = goldenStarSprite;
            }
        }
    }
    public void OpenLevel(int level)
    {
        Time.timeScale = 1;
        string levelName = "Level" + level;
        SceneManager.LoadScene(levelName);
    }
    public void NextLevel()
    {
        
        int unlocklevel = PlayerPrefs.GetInt("UnlockedLevel", 1);
        int nextLevel = SceneManager.GetActiveScene().buildIndex + 1;
        string levelName = "Level" + nextLevel;

        if (nextLevel <= levelObjects.Length)
        {
            Time.timeScale = 1;
            if (nextLevel == unlocklevel)
            {
                PlayerPrefs.SetInt("UnlockedLevel", nextLevel);
            }
            
            SceneManager.LoadScene(levelName);
        }
        else
        {
            Debug.Log("Bạn đã hoàn thành tất cả các level!");
        }
    }
}
