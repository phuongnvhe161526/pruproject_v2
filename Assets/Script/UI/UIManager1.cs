
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager1 : MonoBehaviour
{

    [SerializeField] private GameObject menuScreen;
    [SerializeField] private GameObject levelScreen;
    [SerializeField] private GameObject shopScreen;
    [SerializeField] private AudioClip menuSound;
    private void Awake()
    {
        menuScreen.SetActive(true);
        levelScreen.SetActive(false);
        shopScreen.SetActive(false);
    }
    public void GameOver()
    {
        menuScreen.SetActive(false);
    }
    public void ShowLevelScreen()
    {
        menuScreen.SetActive(false);
        levelScreen.SetActive(true);
    }

    public void Play()
    {
        ShowLevelScreen();
        //SceneManager.LoadScene(1);
    }
    public void Options()
    {
        SceneManager.LoadScene(0);
    }
    public void Shop()
    {
        menuScreen.SetActive(false);
        shopScreen.SetActive(true);
    }
    public void Quit()
    {
       Application.Quit();
        //UnityEditor.EditorApplication.isPlaying = false;
    }
}
