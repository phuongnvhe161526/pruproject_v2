
using UnityEngine;
using UnityEngine.UI;

public class SellectionArrow : MonoBehaviour
{
    [SerializeField] private RectTransform[] options;
    private RectTransform rect;
    private int currentOption;

    private void Awake()
    {
        rect = GetComponent<RectTransform>();

    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            ChangeOption(-1);
        } 
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
        {
            ChangeOption(1);
        }
        if (Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            Interact();
        }
    }

    private void ChangeOption(int _change)
    {
        currentOption += _change;

        if(currentOption < 0)
        {
            currentOption = options.Length-1;
        } else if (currentOption > options.Length-1) {
            currentOption = 0;
        }

        rect.position = new Vector3(rect.position.x, options[currentOption].position.y, 0);
    }
    private void Interact()
    {

        options[currentOption].GetComponent<Button>().onClick.Invoke();
    }
}
