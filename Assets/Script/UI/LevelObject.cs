using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelObject : MonoBehaviour
{
    [SerializeField] public Button levelButton;
    [SerializeField] public Image[] starts;
}
