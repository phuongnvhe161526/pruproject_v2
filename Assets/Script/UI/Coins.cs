using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class Coins : MonoBehaviour
{
    public static Coins Instance;

    public int score;
    private Text coinsText;

    private const string PlayerPrefsKey = "Coins";

    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        coinsText = GetComponent<Text>();
        score = PlayerPrefs.GetInt(PlayerPrefsKey, 0);
        UpdateCoinText();
    }

    public void ChangeScore(int goldValue)
    {
        score += goldValue;
        UpdateCoinText();
    }

    private void UpdateCoinText()
    {
        coinsText.text = "Coins: " + score.ToString();
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.SetInt(PlayerPrefsKey, score);
        PlayerPrefs.Save();
    }

}
