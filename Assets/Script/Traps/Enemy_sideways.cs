using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_sidewways : MonoBehaviour
{
    [SerializeField] private float movement_distance;
    [SerializeField] private float speed;
    [SerializeField] private float damage;
    private bool moving_left;
    private float left_edge;
    private float right_edge;

    private void Awake()
    {
        left_edge = transform.position.x - movement_distance;
        right_edge = transform.position.x + movement_distance;
    }

    private void Update()
    {
        if (moving_left)
        {
            if (transform.position.x > left_edge)
            {
                transform.position = new Vector3(transform.position.x - speed * Time.deltaTime, transform.position.y, transform.position.z);
            }
            else
            {
                moving_left = false;
            }
        } else
        {
            if (transform.position.x < right_edge)
            {
                transform.position = new Vector3(transform.position.x + speed * Time.deltaTime, transform.position.y, transform.position.z);
            }
            else
            {
                moving_left = true;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            collision.GetComponent<Health>().takeDame(damage);
        }
    }
}
