using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowTrap : MonoBehaviour
{
    [SerializeField] private float attackCoolDown;
    [SerializeField] private Transform firePoint;
    [SerializeField] private GameObject[] arrows;
    private float coolDownTimer;

    [Header("SPX")]
    [SerializeField] private AudioClip arrowSound;

    private void Attack()
    {


        coolDownTimer = 0;
        SoundManager.instance.PlaySound(arrowSound);
        arrows[FindArrow()].transform.position = firePoint.position; // reset lai vi tri ban sau sau khi ban
        arrows[FindArrow()].GetComponent<EnemyProjectile>().ActivateProjectile();
    }

    private int FindArrow()
    {
        for(int i=0; i< arrows.Length; i++)
        {
            if (!arrows[i].activeInHierarchy) return i;
        }
        return 0;
    }
    void Update()
    {
        coolDownTimer += Time.deltaTime;

        if(coolDownTimer >= attackCoolDown )
        {
            Attack();
        }
    }

   
}
