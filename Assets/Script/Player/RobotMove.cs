using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;

public class RobotMove : MonoBehaviour
{
    [SerializeField] float speed = 5;
    [SerializeField] float jumpPowwer = 5;
    [SerializeField] LayerMask groundLayer;
    [SerializeField] LayerMask wallLayer;
    private Rigidbody2D body;
    private Animator anim;
    private BoxCollider2D boxCollider;

    [Header("SPX")]
    [SerializeField] private AudioClip jumpSound;

    private void Start()
    {


        body = gameObject.GetComponent<Rigidbody2D>();

        boxCollider = gameObject.GetComponent<BoxCollider2D>();


        GetComponent<SpriteRenderer>().sprite = PlayerSelected.Instance.ChooseChar().GetComponent<SpriteRenderer>().sprite;
        gameObject.GetComponent<Animator>().runtimeAnimatorController = PlayerSelected.Instance.ChooseChar().GetComponent<Animator>().runtimeAnimatorController;
        anim = GetComponent<Animator>();
        GetComponent<BoxCollider2D>().size = PlayerSelected.Instance.ChooseChar().GetComponent<SpriteRenderer>().sprite.bounds.size;
        GetComponent<BoxCollider2D>().offset = Vector2.zero;
    }

    // Update is called once per frame
    void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        body.velocity = new Vector2(horizontalInput * speed, body.velocity.y);
        if (horizontalInput > 0.01f)
        {
            transform.localScale = Vector3.one * 0.4f;
        }
        else if (horizontalInput < -0.01f)
        {
            transform.localScale = new Vector3(-1, 1, 1) * 0.4f;
        }

        if (verticalInput > 0.01f && isGrounded())
        {
            Jump();
        }
        else if (verticalInput < -0.01f)
        {
            Slide();
        }


        //set animator
        anim.SetBool("run", horizontalInput != 0);
        anim.SetBool("isGround", isGrounded());
    }

    private void Jump()
    {
        SoundManager.instance.PlaySound(jumpSound);
        body.velocity = new Vector2(body.velocity.x, jumpPowwer);
        anim.SetTrigger("jump");
    }
    private void Slide()
    {
        //body.velocity = new Vector2(body.velocity.x, speed);
        //anim.SetTrigger("slide");
    }
    private bool isGrounded()
    {
        RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider.bounds.center, boxCollider.bounds.size, 0, Vector2.down, 0.1f, groundLayer);
        return raycastHit.collider != null;
    }
    private bool onWall()
    {
        RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider.bounds.center, boxCollider.bounds.size, 0, new Vector2(transform.localScale.x,0), 0.1f, wallLayer);
        return raycastHit.collider != null;
    }
    public  bool canAttack()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        return horizontalInput == 0;
    }


}
