using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSelected : MonoBehaviour
{
    private static PlayerSelected instance;
    public static PlayerSelected Instance { get => instance; }


    // Start is called before the first frame update
    public GameObject[] skins;
    public int playerSelected;
    public Character[] characters;

    public Button unlockButton;

    public Text nameText;
    public Text coinsText;


    public static Vector2 lastCheckPoint = new Vector2(-3, 0);

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }

        playerSelected = PlayerPrefs.GetInt("SelectedCharacter", 0);

        foreach (GameObject player in skins)
            player.SetActive(false);

        skins[playerSelected].SetActive(true);

        foreach (Character c in characters)
        {
            if (c.price == 0)
                c.isUnlocked = true;
            else
            {
                c.isUnlocked = PlayerPrefs.GetInt(c.name, 0) == 0 ? false : true;
            }
        }
        UpdateUI();


    }

    public void ChangeNext()
    {
        skins[playerSelected].SetActive(false);
        playerSelected++;
        if (playerSelected == skins.Length)
            playerSelected = 0;

        skins[playerSelected].SetActive(true);

        if (characters[playerSelected].isUnlocked)
            PlayerPrefs.SetInt("SelectedCharacter", playerSelected);

        UpdateUI();
    }

    public void ChangePrevious()
    {
        skins[playerSelected].SetActive(false);
        playerSelected--;
        if (playerSelected == -1)
            playerSelected = skins.Length - 1;

        skins[playerSelected].SetActive(true);
        if (characters[playerSelected].isUnlocked)
            PlayerPrefs.SetInt("SelectedCharacter", playerSelected);

        UpdateUI();
    }

    public void UpdateUI()
    {
        coinsText.text = "Coins: " + PlayerPrefs.GetInt("Coins", 0);
        //nameText.text = characters[playerSelected].name;
        if (characters[playerSelected].isUnlocked == true)
            unlockButton.gameObject.SetActive(false);
        else
        {
            unlockButton.GetComponentInChildren<Text>().text = "Price:" + characters[playerSelected].price;
            if (PlayerPrefs.GetInt("Coins", 0) < characters[playerSelected].price)
            {
                unlockButton.gameObject.SetActive(true);
                unlockButton.interactable = false;
            }
            else
            {
                unlockButton.gameObject.SetActive(true);
                unlockButton.interactable = true;
            }
        }
    }

    public void Unlock()
    {
        int coins = PlayerPrefs.GetInt("Coins", 0);
        int price = characters[playerSelected].price;
        PlayerPrefs.SetInt("Coins", coins - price);
        PlayerPrefs.SetInt(characters[playerSelected].name, 1);
        PlayerPrefs.SetInt("SelectedCharacter", playerSelected);
        characters[playerSelected].isUnlocked = true;

        UpdateUI();
    }

    public GameObject ChooseChar()
    {
        int num = PlayerPrefs.GetInt("SelectedCharacter");
        GameObject player = skins[num];
        return player;
    }
}