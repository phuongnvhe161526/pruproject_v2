using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCollection : MonoBehaviour
{
    // Start is called before the first frame update
    public int goldValue = 1;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Coins.Instance.ChangeScore(goldValue);
            Destroy(gameObject);
        }
    }

}
