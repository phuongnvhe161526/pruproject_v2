
using UnityEngine;

public class EnermyPatrol : MonoBehaviour
{
    [Header("Patrol points")]
    [SerializeField] private Transform leftEdge;
    [SerializeField] private Transform rightEdge;

    [Header("Enermy")]
    [SerializeField] private Transform enermy;

    [Header("Movement params")]
    [SerializeField] private float speed;
    private Vector3 initScale;
    private bool movingLeft;

    [Header("Idle behavior")]
    [SerializeField] private float idleDuration;
    private float idleTimer;


    [Header("Enermy")]
    [SerializeField] private Animator animator;

    private void Awake()
    {
        initScale = enermy.localScale;
    }
    private void OnDisable()
    {
        animator.SetBool("moving", false);
    }
    private void Update()
    {
        if (movingLeft)
        {
            if (enermy.position.x >= leftEdge.position.x)
                MoveInDirection(-1);
            else
                DirectionChange();

        }
        else
        {
            if (enermy.position.x <= rightEdge.position.x)
                MoveInDirection(1);
            else
                DirectionChange();
        }
    }
    private void DirectionChange()
    {
        animator.SetBool("moving", false);

        idleTimer += Time.deltaTime;
        if(idleTimer > idleDuration)
             movingLeft = !movingLeft;
    }
    private void MoveInDirection(int direction)
    {
        idleTimer = 0;
        animator.SetBool("moving", true);
        enermy.localScale = new Vector3(Mathf.Abs(initScale.x) * direction, initScale.y, initScale.z);

        enermy.position = new Vector3(enermy.position.x + Time.deltaTime * direction * speed, enermy.position.y, enermy.position.z);

    }
}
