
using UnityEngine;

public class MeleeEnermy : MonoBehaviour
{
    [SerializeField] private float attackCooldown;
    [SerializeField] private float range;
    [SerializeField] private float coliderDistance;
    [SerializeField] private int damage;
    [SerializeField] private BoxCollider2D boxCollider2D;
    [SerializeField] private LayerMask playerLayer;
    private float cooldownTimer  = Mathf.Infinity;

    [Header("Attack Sound")]
    [SerializeField] private AudioClip attackSound;

    private EnermyPatrol enermyPatrol;

    private Animator animator;
    private Health health;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        enermyPatrol = GetComponentInParent<EnermyPatrol>();
    }
    private void Update()
    {
        cooldownTimer += Time.deltaTime;

        if(PlayerInSight())
        {
            if (cooldownTimer > attackCooldown)
            {
                cooldownTimer = 0;
                animator.SetTrigger("meleeAttack");
                SoundManager.instance.PlaySound(attackSound);
            }

        }
        if(enermyPatrol!= null) 
            enermyPatrol.enabled = !PlayerInSight();
       
            //Attack();
        
    }
    private bool PlayerInSight()
    {
        RaycastHit2D hit = Physics2D.BoxCast(boxCollider2D.bounds.center + transform.right * range * transform.localScale.x * coliderDistance,
           new Vector3(boxCollider2D.bounds.size.x * range, boxCollider2D.bounds.size.y,boxCollider2D.bounds.size.z)
            , 0,Vector2.left,0,playerLayer);
        if(hit.collider != null)
        {
            health = hit.transform.GetComponent<Health>();
        }
        return hit.collider != null; 
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(boxCollider2D.bounds.center + transform.right * range * transform.localScale.x * coliderDistance
            , new Vector3(boxCollider2D.bounds.size.x * range, boxCollider2D.bounds.size.y, boxCollider2D.bounds.size.z));
    }

    private void DamagePlayer()
    {
        if(PlayerInSight())
        {
            health.takeDame(damage);
        }
    }
}
