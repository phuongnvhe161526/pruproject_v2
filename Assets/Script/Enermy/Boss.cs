using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    [Header("Idle")]
    [SerializeField] float idleMoveSpeed;
    [SerializeField] Vector2 idleMoveDirection;

    [Header("AttackUpDown")]
    [SerializeField] float attackMoveSpeed;
    [SerializeField] Vector2 attackMoveDirection;

    [Header("AttackPlayer")]
    [SerializeField] float attackPlayerSpeed;
    [SerializeField] Transform player;
    private Vector2 playerPosition;
    private bool hasPlayerPosition;

    [Header("Other")]
    [SerializeField] Transform groundCheckUp;
    [SerializeField] Transform groundCheckDown;
    [SerializeField] Transform groundCheckWall;
    [SerializeField] float groundCheckRadius;
    [SerializeField] LayerMask groundLayer;
   

    private bool isTouchingUp;
    private bool isTouchingDown;
    private bool isTouchingWall;
    private bool goingUp = true;
    private bool facingLeft = true;
    public  bool isSameRoom = false;
    private Rigidbody2D enemyRB;
    private Animator animator;
    private void Start()
    {
        idleMoveDirection.Normalize();
        attackMoveDirection.Normalize();
        enemyRB = GetComponent<Rigidbody2D>();
    }
    private void Awake()
    {
        animator = gameObject.GetComponent<Animator>();
    }

    private void Update()
    {
        isTouchingUp = Physics2D.OverlapCircle(groundCheckUp.position, groundCheckRadius, groundLayer);
        isTouchingDown = Physics2D.OverlapCircle(groundCheckDown.position, groundCheckRadius, groundLayer);
        isTouchingWall = Physics2D.OverlapCircle(groundCheckWall.position, groundCheckRadius, groundLayer);
        //IdleState();
        //FlipTowardsPlayer();
        
    }
    void randomStatePicker()
    {
        if(isSameRoom)
        {
            int randomState = Random.Range(0, 4);
            if (randomState == 0)
            {
                animator.SetTrigger("AttackUpNDown");
            }
            else if (randomState == 1)
            {
                animator.SetTrigger("AttackPlayer");
            }
        }
    }
    public void IdleState()
    {
        if(isTouchingUp && goingUp)
        {
            ChangeDirection();
        } else if(isTouchingDown && !goingUp)
        {
            ChangeDirection();
        }
        if(isTouchingWall)
        {
            if(facingLeft)
            {
                Flip();
            } else if (!facingLeft)
            {
                Flip();
            }
        }
        enemyRB.velocity = idleMoveSpeed * idleMoveDirection;
        
    }
    public void AttackUpdown()
    {
        if (isTouchingUp && goingUp)
        {
            ChangeDirection();
        }
        else if (isTouchingDown && !goingUp)
        {
            ChangeDirection();
        }
        if (isTouchingWall)
        {
            if (facingLeft)
            {
                Flip();
            }
            else if (!facingLeft)
            {
                Flip();
            }
        }
        enemyRB.velocity = attackMoveDirection * attackMoveSpeed;

    }

    public void FlipTowardsPlayer()
    {
        if (isSameRoom)
        {
            float playerDirecton = player.position.x - transform.position.x;

            if (playerDirecton > 0 && facingLeft)
            {
                Flip();
            }
            else if (playerDirecton < 0 && !facingLeft)
            {
                Flip();
            }
        }
    }
    public void AttackPlayer()
    {
        if(!hasPlayerPosition)
        {
            playerPosition = player.position - transform.position;

            playerPosition.Normalize();
            hasPlayerPosition = true;
        } 
        if(hasPlayerPosition)
        {
            enemyRB.velocity = playerPosition * attackPlayerSpeed;
        }
        if(isTouchingDown || isTouchingWall)
        {
            //enemyRB.position = Vector2.zero;
            enemyRB.velocity = Vector2.zero;
           hasPlayerPosition = false;
            animator.SetTrigger("Slamed");
        }
       

    }
    void ChangeDirection()
    {
        goingUp = !goingUp;
        idleMoveDirection.y *= -1;
        attackMoveDirection.y *= -1;
    }
    void Flip()
    {
        facingLeft = !facingLeft;
        idleMoveDirection.x *= -1;
        attackMoveDirection.x *= -1;
        transform.Rotate(0, 180, 0);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(groundCheckUp.position, groundCheckRadius);
        Gizmos.DrawWireSphere(groundCheckDown.position, groundCheckRadius);
        Gizmos.DrawWireSphere(groundCheckWall.position, groundCheckRadius);
    }
}
