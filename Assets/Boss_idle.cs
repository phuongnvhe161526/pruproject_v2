using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_idle : StateMachineBehaviour
{
    [SerializeField] Boss boss;
    private bool isAnimationFinished = false;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        boss = GameObject.FindGameObjectWithTag("Boss").GetComponent<Boss>();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        boss.IdleState(); 
        if (stateInfo.normalizedTime >= 1f)
        {
            isAnimationFinished = true;
        }

        if (isAnimationFinished)
        {
            animator.Play("Boss_Idle", layerIndex, 0f); 
            isAnimationFinished = false;
        }
    }
}
